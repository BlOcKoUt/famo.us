var config = {
  silent: false,
  fatal: false
};
exports.config = config;

function log() {
	console.log.apply(this, arguments);
}
exports.log = log;

function error(msg, _continue) {
  if (state.error === null)
    state.error = '';
  state.error += state.currentCmd + ': ' + msg + '\n';

  if (msg.length > 0)
    log(state.error);

  if (config.fatal)
    process.exit(1);

  if (!_continue)
    throw '';
}
exports.error = error;

var state = {
  error: null,
  currentCmd: 'ls-js',
  tempDir: null
};
exports.state = state;

function ShellString(str) {
  return str;
}
exports.ShellString = ShellString;

function parseOptions(str, map) {
  if (!map)
    error('parseOptions() internal error: no map given');

  var options = {};
  for (var letter in map)
    options[map[letter]] = false;

  if (!str)
    return options;

  if (typeof str !== 'string')
    error('parseOptions() internal error: wrong str');

  var match = str.match(/^\-(.+)/);
  if (!match)
    return options;

  var chars = match[1].split('');

  chars.forEach(function(c) {
    if (c in map)
      options[map[c]] = true;
    else
      error('option not recognized: '+c);
  });

  return options;
}
exports.parseOptions = parseOptions;

function wrap(cmd, fn, options) {
  return function() {
    var retValue = null;

    state.currentCmd = cmd;
    state.error = null;

    try {
      var args = [].slice.call(arguments, 0);

      if (options && options.notUnix) {
        retValue = fn.apply(this, args);
      } else {
        if (args.length === 0 || typeof args[0] !== 'string' || args[0][0] !== '-')
          args.unshift('');
        retValue = fn.apply(this, args);
      }
    } catch (e) {
      if (!state.error) {
        console.log('ls-js: internal error');
        console.log(e.stack || e);
        process.exit(1);
      }
      if (config.fatal)
        throw e;
    }

    state.currentCmd = 'ls-js';
    return retValue;
  };
}
exports.wrap = wrap;
