var lodash = require('lodash');
var userid = require('userid');

function _display_results(results, option) {
	
	if (option == "list") {
		
		var months = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		
		var arrayLength = results.length;
		for (var i = 0; i < arrayLength; i++) {
			
			date = new Date(results[i]['mtime']);
			month = months[date.getMonth()];
			day = date.getDate();
			if(day < 10)
				day = "0" + day;
			year = date.getFullYear();
			hours = date.getHours();
			if(hours < 10)
				hours = "0" + hours;
			minutes = date.getMinutes();
			if(minutes < 10)
				minutes = "0" + minutes;
			finalDate = month + " " + day + " " + year + " " + hours + ":" + minutes;
			
			
			if (results['blocks'] == 0) {
				perms = "d";
			} else {
				perms = "-";
			}
			mode = results[i]['mode'].toString(8);
			mode = mode.substr(mode.length - 3);
			for (var j = 0; j < 3; j++) {
				switch(mode[j]) {
				    case "0":
				        perms += "---";
				        break;
				    case "1":
				        perms += "--x";
				        break;
					  case "2":
					      perms += "-w-";
					      break;
					  case "3":
					      perms += "-wx";
					      break;
					  case "4":
					      perms += "r--";
			        break;
			      case "5":
				        perms += "r-x";
					      break;
						case "6":
						    perms += "rw-";
					      break;
						case "7":
							  perms += "rwx";
						    break;
				}
			}
			
			process.stdout.write(perms + "  " + results[i]['nlink'] + " " + userid.username(results[i]['uid']) + "  " + userid.groupname(results[i]['gid']) + "\t" + results[i]['size'] + "\t" + finalDate + " " + results[i]['name'] + "\n");

		}


	} else {

		names = lodash.map(results, 'name');
		var arrayLength = names.length;
		for (var i = 0; i < arrayLength; i++) {
			process.stdout.write(names[i] + "\n");
		}

	}

}

module.exports = _display_results;
