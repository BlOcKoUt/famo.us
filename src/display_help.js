function _display_help() {
	var message = [
'NAME ',
'     ls-js -- list directory contents',
'',
'SYNOPSIS',
'     ls [-larh] [file ...]',
'',
'DESCRIPTION',
'     For each operand that names a file of a type other than directory, ls displays its name as well as any requested, associated information.  For each operand that names a file of type directory, ls displays the',
'     names of files contained within that directory, as well as any requested, associated information.',
'',
'     If no operands are given, the contents of the current directory are displayed.',
'',
'     The following options are available:',
'',
'		 -l      (The lowercase letter "ell".)  List in long format.',
'		 -a      Include directory entries whose names begin with a dot (.).',
'		 -r      Recursively list subdirectories encountered.',
'		 -h      Help message.',
'',
].join('\n');

	console.log(message);
}
module.exports = _display_help;