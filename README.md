Welcome to the wonderful world of tools!

Your task, if you choose to accept it, is to replicate the classic unix command 'ls' completely in node.js!

Your finished product should be able to be installed globally via npm, and expose a script ls-js that exhibits the same functionality as the classic ls command.  This needs to be done without shelling out, so no using child.exec.  This also cannot be done using any npm modules that offer this express functionality, such as shell.js.

I was able to accomplish this task using only lodash (for list manipulation) and minimist (for command line argument parsing).

This problem can be solved, albeit extremely naively, in about 15 minutes.  A more robust solution, including input sanitation, pretty printed output, support for various flags, testing, and continuous integration could potentially take days.  It is worth mentioning that we are not expecting you to completely replicate EVERY flag that is offered by ls, -l for example is almost an entire app in itself.  That being said certain flags such as -a or -g can potentially be quick wins.

When you are complete please email a tarball of your repo (including git commit information) back to Mei within 24 hours.


### Solution ###

Installation instructions:

npm install -g [ This will install ls-js and it's dependencies (minimist, lodash, userid) ]

Usage:

ls [-larh] [file/directory ...]

I have implemented -l -a -R and -h switches (case insensitive)
  -l      (The lowercase letter "ell".)  List in long format.
  -a      Include directory entries whose names begin with a dot (.).'
  -r      Recursively list subdirectories encountered.
  -h      Help message.

If script is used without arguments, it will display content of current directory in short format, just list of files.
e.g. ls-js

If script is used with -l switch, it will display content of current directory, in long format.
e.g. ls-js -l

If script is used with -a switch, it will display content of current directory together with files whose names begin with a dot.
e.g. ls-js -a

If script is used with -h switch, it will display a help message.
e.g. ls-js -h

Switches can be combined, so you can use e.g. ls-js -lar . (This will recursively display all files starting from current directory, in log format. Files starting with dot will be included)

